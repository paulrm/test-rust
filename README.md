```
 ____            _   
|  _ \ _   _ ___| |_ 
| |_) | | | / __| __|
|  _ <| |_| \__ \ |_ 
|_| \_\\__,_|___/\__|

```
Test de Hello World en Rust

El Make tiene dos metodos de resolverse

1. Metodo convencional
```
make all     <- Comand 
    rustc hello.rs     <- compila
    ./hello            <- corrida
    Hello World!       <- output
```

1. Metodo con Cargo
  1. Requerimientos
    1. debe existir el main.rs en el dir src
    1. debe existir el archivo de config Cargo.toml

```
cargo build <-
   Compiling hello_cargo v0.1.0 (file:///home/paul/test-rust)
    Finished dev [unoptimized + debuginfo] target(s) in 0.51 secs

cargo run <-
    Finished dev [unoptimized + debuginfo] target(s) in 0.0 secs
     Running `target/debug/hello_cargo`
Hello World!     <- output
```
# Pendientes
- [ ] Agregar un test unitario
