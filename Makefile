build:
	cargo build

hello: hello.rs 
	rustc hello.rs

clean:
	rm -f ./hello

run:
	./hello

all: hello run
	
